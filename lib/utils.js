"use strict";

const SHA = require('js-sha256')
const PROMISE = require('promise');

module.exports = {
  validateMail: function(str)
  {
    const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(str);
  },
  validatePass: function(str)
  {
    if(str) {
      let length = str.length>=8;
      let upper = /[A-Z]/.test(str);
      let lower = /[a-z]/.test(str);
      let number = /\d/.test(str);
      return length && upper && lower && number;
    } else {
      return false;
    }
  },
  validateLogin: function(str)
  {
    if(str) {
      let min = str.length>=3;
      let max = str.length<=30;
      return min && max;
    } else {
      return false;
    }
  },
  generateToken: function(req, mail)
  {
    let str = "";
    str += req.headers["from-within"];
    str += req.headers["user-agent"];
    str += req.headers["x-forwarded-for"];
    str += mail;
    return SHA(str);
  },
  readCookie: function(req)
  {
    let tmp;
    let str = req.headers["cookie"];
    str = str.split(";");
    str = str.map(function(element, index, array)
        {
          return element.split("=");
        });
    str = str.map(function(element, index, array)
        {
          return element.map(function(element2, index2, array2)
              {
                return element2.trim();
              });
        });
    tmp = str.find(function(element, index, array)
        {
          return element[0]==="data";
        });
    if(tmp) {
      return tmp[1];
    } else {
      return null;
    }
  },
  getAuthorityLevel: function(authority)
  {
    switch(authority) {
      case 'MasterAdmin':
        return 3;
        break;
      case 'Admin':
        return 2;
        break;
      case 'Moderator':
        return 1;
        break;
      case 'User':
        return 0;
        break;
      default:
        return -1;
        break;
    }
  },
  canModify: function(datas)
  {
    if(this.getAuthorityLevel(datas["selfAUTHORITY"])<this.getAuthorityLevel("Admin")) return false;
    console.log(datas);
    switch(datas["action"]) {
      case 'avatarchangeform':
        return false;
        break;
      case 'authoritychangeform':
        if(datas["authority"]=="MasterAdmin") return false;
        if(datas["userID"]==0 && datas["authority"]!="MasterAdmin") return false;
        if(datas["selfID"]==datas["userID"]) return false;
        if(this.getAuthorityLevel(datas["authority"])>this.getAuthorityLevel(datas["selfAUTHORITY"])) return false;
        if(this.getAuthorityLevel(datas["selfAUTHORITY"])<this.getAuthorityLevel(datas["userAUTHORITY"])) return false;
        break;
      case 'loginchangeform':
        if(datas["login1"]!=datas["login2"]) return false;
        if(!this.validateLogin(datas["login1"])) return false;
        break;
      case 'passchangeform':
        if(datas["pass1"]!=datas["pass2"]) return false;
        if(this.getAuthorityLevel(datas["selfAUTHORITY"])<this.getAuthorityLevel("MasterAdmin")) return false;
        if(!this.validatePass(datas["pass1"])) return false;
        break;
      default:
        valid = false;
        break;
    }
    return true;
  },
  signUser: function(db, datas)
  {
    const OBJ = {};
    const that = this;
    switch(datas['action']) {
      case 'signformin':
        return new PROMISE(function(fulfill, reject)
            {
              db.collection("users").find(
                  { mail: datas['mail1'], pass: SHA(datas['pass1']) }
                  ).toArray()
                .then(function(data)
                    {
                      fulfill(data[0]);
                    })
              .catch(function(err)
                  {
                    reject(err);
                  });
            });
        break;
      case 'signformup':
        return new PROMISE(function(fulfill, reject)
            {
              db.collection("users").find().sort({_id: -1}).limit(1).toArray()
                .then(function(data1)
                    {
                      const USER = {
                        id:  (data1[0]['_id'] + 1),
                        mail: datas['mail1'],
                        pass: SHA(datas['pass1']),
                        login: 'USER'+(data1[0]['_id']+1),
                        avatar: 'media/images/avatars/avatar_default.png',
                        authority: 'User'}
                      db.collection("users").insert(
                          { _id: USER.id,
                            mail: USER.mail,
                            pass: USER.pass,
                            login: USER.login,
                            avatar: USER.avatar,
                            authority: USER.authority,
                            tokens: []
                          }
                          )
                        .then(function(data2)
                            {
                              fulfill(USER);
                            })
                      .catch(function(err)
                          {
                            reject(err);
                          });
                    })
              .catch(function(err)
                  {
                    reject(err);
                  });
            });
        break;
      case 'signformresetpass':
        return new PROMISE(function(fulfill, reject)
            {
              console.log(datas['newpass']);
              db.collection("users").update(
                  { mail: datas['mail1'] },
                  { $set: {pass: SHA(datas["newpass"])} }
                  )
                .then(function(data)
                    {
                      fulfill(true);
                    })
              .catch(function(err)
                  {
                    reject(err);
                  });
            });
        break;
      default:
        return new PROMISE(function(fulfill, reject)
            {
              reject("action error");
            });
        break;
    }
  },
  changeUserData: function(db, datas)
  {
    const that = this;
    switch(datas['action']) {
      case 'avatarchangeform':

        break;
      case 'authoritychangeform':
        return new PROMISE(function(fulfill, reject)
            {
              db.collection("users").update(
                  { _id: datas['userID'] },
                  { $set: {authority: datas['authority']} })
              .then(function(data)
                  {
                    fulfill(true);
                  })
              .catch(function(err)
                  {
                    console.error(err);
                    reject(err);
                  });
            });
        break;
      case 'loginchangeform':
        return new PROMISE(function(fulfill, reject)
            {
              db.collection("users").update(
                  { _id: datas['userID'] },
                  { $set: {login: datas['login1']} })
              .then(function(data)
                  {
                    fulfill(true);
                  })
              .catch(function(err)
                  {
                    console.error(err);
                    reject(err);
                  });
            });
        break;
      case 'passchangeform':
        return new PROMISE(function(fulfill, reject)
            {
              db.collection("users").update(
                  { _id: datas['userID'] },
                  { $set: {pass: SHA(datas['pass1'])} })
              .then(function(data)
                  {
                    fulfill(true);
                  })
              .catch(function(err)
                  {
                    console.error(err);
                    reject(err);
                  });
            });
        break;
      default:
        return new PROMISE(function(fulfill, reject)
            {
              reject("action error");
            });
        break;
    }
  },
  validateChangeData: function(db, datas)
  {
    const that = this;
    return new PROMISE(function(fulfill, reject)
        {
          if(that.canModify(datas)) {
            db.collection('users').find({_id: datas['selfID'], authority: datas['selfAUTHORITY']}).toArray()
              .then(function(data1)
                  {
                    if(data1.length===1) {
                      db.collection('users').find({_id: datas['userID'], authority: datas['userAUTHORITY']}).toArray()
                        .then(function(data2)
                            {
                              fulfill(true);
                            })
                      .catch(function(err)
                          {
                            console.error(err);
                            reject(err);
                          });
                    } else {
                      fulfill(false);
                    }
                  })
            .catch(function(err)
                {
                  console.error(err);
                  reject(err);
                });
          } else {
            fulfill(false);
          }
        });
  },
  validateSignData: function(db, datas)
  {
    const that = this;
    return new PROMISE(function(fulfill, reject)
        {
          db.collection('users').find({mail: datas['mail1']}).toArray()
            .then(function(data)
                {
                  fulfill(data.length);
                })
          .catch(function(err)
              {
                reject(err);
              });
        });
  },
  findLogin: function(id, users)
  {
    const user = users.find(function(element, index, array)
        {
          return element['_id'] == id;
        });
    return user.login;
  }
};
