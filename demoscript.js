"use strict";

const DEV = process.env.NODE_ENV !== 'production';

const UTILS = require("./lib/utils.js");

const BODYPARSER = require('body-parser');
const SHA = require('js-sha256');
const PATH = require('path');
const URL = require('url');
const MongoClient = require('mongodb').MongoClient;

const MONGOURL = process.env.MONGO_URL;
const PORT = process.env.PORT || '8081';

const EXPRESS = require('express');
const app = EXPRESS();
app.use(EXPRESS.static(PATH.join(__dirname, "client")));
app.use(BODYPARSER.json());                       // to support JSON-encoded bodies
app.use(BODYPARSER.urlencoded({extended: true})); // to support URL-encoded bodies

let USERS = [];

const refreshUSERS = function()
{
  DB.collection("users").find().toArray()
    .then(function(data)
        {
          USERS = data;
        })
  .catch(function(err)
      {
        console.error(err);
      });
}
const treatStoriesData = function(data)
{
  let tmp = data;

  tmp.forEach(function(element, index, array)
      {
        element.login = UTILS.findLogin(element.author, USERS);
        element['actions'].forEach(function(element2, index2, array2)
            {
              element2.login = UTILS.findLogin(element2.author, USERS);
            });
        element['candidates'].forEach(function(element2, index2, array2)
            {
              element2.login = UTILS.findLogin(element2.author, USERS);
            });
      });

  return tmp;
}

let DB;
MongoClient.connect(MONGOURL, {})
.then(function(db)
    {
      DB = db;
      refreshUSERS();
      app.listen(PORT);
    })
.catch(function(err)
    {
      console.log(err);
    });


app.all("*", function(err, req, res, next)
    {
      if(req.headers["from-within"] && req.headers["from-within"]==="from-within") {
        next();
      } else {
        console.error(err.stack);
        res.sendStatus(403);
      }
    });

//Log
//GET: send template file for when arriving
//POST: connect user if connection by cookie, return userFound (if OK)
  app.route("/log")
.get(function(req, res)
    {
      res.sendFile('log/before.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const OBJ = {};
      const cookie = UTILS.readCookie(req).split("|");
      const MAIL = cookie[0];
      const TOKEN = cookie[1];
      const tok = UTILS.generateToken(req, MAIL);

      if(tok===TOKEN) {
        DB.collection("users").find({tokens: {$elemMatch: {$eq: TOKEN}}}).toArray()
          .then(function(data)
              {
                if(data.length===1) {
                  const USER = {
                    id: data[0]["_id"],
                    authority: data[0]["authority"],
                    login: data[0]["login"],
                    avatar: data[0]["avatar"]
                  }
                  OBJ.userFound = true;
                  OBJ.USER = USER;
                  res.end(JSON.stringify(OBJ));
                } else if(data.length>1) {
                  OBJ.tooMany = true;
                  res.end(JSON.stringify(OBJ));
                } else {
                  res.end(JSON.stringify(OBJ));
                }
              })
        .catch(function(err)
            {
              console.error(err);
              res.sendStatus(500);
            });
      } else {
        OBJ.notSame = true;
        res.end(JSON.stringify(OBJ));
      }
    });

//Logged
//GET: send template file for when logged
//POST: create token for cookies, return userValid (if OK)
  app.route("/logged")
.get(function(req, res)
    {
      res.sendFile('log/after.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const OBJ = {};
      const datas = JSON.parse(Object.keys(req.body)[0]);

      const tok = UTILS.generateToken(req, datas["mail"]);

      DB.collection("users").find({_id: datas["id"]}).toArray()
        .then(function(data)
            {
              if(data.length===1) {
                if(data[0]["mail"]===datas["mail"]) {
                  OBJ.userValid = true;
                  DB.collection("users").update(
                      {_id: datas["id"]},
                      {$addToSet: {tokens: tok}})
                  .then(function(data)
                      {
                        res.end(tok);
                      })
                  .catch(function(err)
                      {
                        console.error(err);
                        res.sendStatus(500);
                      });
                } else {
                  OBJ.invalid = true;
                  res.sendStatus(400);
                }
              } else {
                res.sendStatus(400);
              }
            })
      .catch(function(err)
          {
            console.error(err);
            res.sendStatus(500);
          });
    });

//sign
//GET: send corresponding template file
//POST: check data for signing in, signing up and resetting password
  app.route("/sign/*")
.get(function(req, res, next)
    {
      next();
    })
.post(function(req, res, next)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);

      UTILS.validateSignData(DB, datas)
        .then(function(number)
            {
              req['signdata'] = number;
              next();
            })
      .catch(function(err)
          {
            console.log(err);
            res.sendStatus(500);
          });

    });

//Signin
//GET: send template file for signing in
//POST: returns user found, 400 if not, 500 if error
  app.route("/sign/in")
.get(function(req, res)
    {
      res.sendFile('sign/in.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const OBJ = {};
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const number = req['signdata'];

      if(number===1) {
        UTILS.signUser(DB, datas)
          .then(function(data)
              {
                refreshUSERS();
                if(data) {
                  OBJ.id = data._id;
                  OBJ.authority = data.authority;
                  OBJ.login = data.login;
                  OBJ.avatar = data.avatar;
                  if(datas["cookie"]===true) OBJ.mail = data.mail;
                  res.end(JSON.stringify(OBJ));
                } else {
                  res.sendStatus(400);
                }
              })
        .catch(function(err)
            {
              console.log(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(400);
      }
    });

//Resetpass
//GET: send template for resetting password (from signing in)
//POST: returns 200 if password reset, 400 if not, 500 if error
  app.route("/sign/resetpass")
.get(function(req, res)
    {
      res.sendFile('sign/resetpass.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const number = req['signdata'];

      if(number===1) {
        UTILS.signUser(DB, datas)
          .then(function(data)
              {
                refreshUSERS();
                if(data) {
                  res.sendStatus(200);
                } else {
                  res.sendStatus(400);
                }
              })
        .catch(function(err)
            {
              console.log(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(400);
      }
    });

//Signup
//GET: send template file for signing up
//POST: returns user created, 400 if not, 500 if error
  app.route("/sign/up")
.get(function(req, res)
    {
      res.sendFile('sign/up.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const OBJ = {};
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const number = req['signdata'];

      if(number===0) {
        UTILS.signUser(DB, datas)
          .then(function(data)
              {
                refreshUSERS();
                if(data) {
                  OBJ.id = data.id;
                  OBJ.authority = data.authority;
                  OBJ.login = data.login;
                  OBJ.avatar = data.avatar;
                  if(datas["cookie"]===true) OBJ.mail = data.mail;
                  res.end(JSON.stringify(OBJ));
                } else {
                  res.sendStatus(400);
                }
              })
        .catch(function(err)
            {
              console.log(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(400);
      }
    });

//Userinfo
//GET: send template file for user's infos
  app.route("/pages/userinfo")
.get(function(req, res)
    {
      res.sendFile('pages/userinfo.html', {root: PATH.join(__dirname, 'client/views/')});
    });

//Change
//GET: send corresponding template file
//POST: check data for changing user info
  app.route("/change/*")
.get(function(req, res, next)
    {
      next();
    })
.post(function(req, res, next)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);

      UTILS.validateChangeData(DB, datas)
        .then(function(valid)
            {
              req['changedata'] = valid;
              next();
            })
      .catch(function(err)
          {
            console.log(err);
            res.sendStatus(500);
          });

    });

//Changepass
  app.route("/change/pass")
.get(function(req, res)
    {
      res.sendFile('change/pass.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const valid = req['changedata'];

      if(valid) {
        UTILS.changeUserData(DB, datas)
          .then(function(data)
              {
                res.end(JSON.stringify(data));
              })
        .catch(function(err)
            {
              console.error(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(403);
      }
    });

//Changelogin
  app.route("/change/login")
.get(function(req, res)
    {
      res.sendFile('change/login.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const valid = req['changedata'];

      if(valid) {
        UTILS.changeUserData(DB, datas)
          .then(function(data)
              {
                res.end(JSON.stringify(data));
              })
        .catch(function(err)
            {
              console.error(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(403);
      }
    });

//Changeavatar
  app.route("/change/avatar")
.get(function(req, res)
    {
      res.sendFile('change/avatar.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const valid = req['changedata'];

      if(valid) {
        UTILS.changeUserData(DB, datas)
          .then(function(data)
              {
                res.end(JSON.stringify(data));
              })
        .catch(function(err)
            {
              console.error(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(403);
      }
    });

//Changeauthority
  app.route("/change/authority")
.get(function(req, res)
    {
      res.sendFile('change/authority.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);
      const valid = req['changedata'];

      if(valid) {
        UTILS.changeUserData(DB, datas)
          .then(function(data)
              {
                res.end(JSON.stringify(data));
              })
        .catch(function(err)
            {
              console.error(err);
              res.sendStatus(500);
            });
      } else {
        res.sendStatus(403);
      }
    });

//MainPage
//GET: send template file for main page
  app.route("/pages/mainpage")
.get(function(req, res)
    {
      res.sendFile('pages/mainpage.html', {root: PATH.join(__dirname, 'client/views/')});
    });

//New Story
//GET: send template file for new/edit story/block/chapter
//POST: create new story
  app.route("/pages/mainpage/stories/new")
.get(function(req, res)
    {
      res.sendFile('demoscript/new.html', {root: PATH.join(__dirname, 'client/views/')});
    })
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);
      if(datas['userID']!=null && datas['userID']!=undefined) {
        DB.collection("users").find({_id: datas['userID']}).toArray()
          .then(function(user)
              {
                if(user.length===1) {
                  if(user[0]['login']==datas['userLOGIN'] && user[0]['authority']==datas['userAUTHORITY']) {
                    switch(datas['action']) {
                      case 'new':
                        if(datas['storyref']) {
                          if(datas['content']) {
                            DB.collection("stories").find({_id: datas['storyref']}).toArray()
                              .then(function(data)
                                  {
                                    if(data.length===1) {
                                      const NUM = data[0]['actions'].length;
                                      let LAST = null;
                                      if(NUM>0) LAST = data[0]['actions'][NUM-1];
                                      let chain = datas['userID'] + NUM;
                                      if(LAST) chain += LAST['chain'];
                                      const OBJ = {
                                        number: NUM,
                                        chain: SHA(chain),
                                        author: datas['userID'],
                                        login: datas['userLOGIN'],
                                        title: datas['title'],
                                        content: datas['content'],
                                        action: datas['action']
                                      }
                                      DB.collection("stories").update(
                                          {_id: datas['storyref']},
                                          {$addToSet: {actions: OBJ}}
                                          )
                                        .then(function(data2)
                                            {
                                              res.end(JSON.stringify(OBJ));
                                            })
                                      .catch(function(err)
                                          {
                                            console.error(err);
                                            res.sendStatus(500);
                                          });
                                    } else {
                                      res.sendStatus(500);
                                    }
                                  })
                            .catch(function(err)
                                {
                                  console.error(err);
                                  res.sendStatus(500);
                                });
                          } else {
                            res.sendStatus(500);
                          }
                        } else {
                          if(datas['title'] && datas['content']) {
                            DB.collection("stories").find().sort({_id: -1}).limit(1).toArray()
                              .then(function(data)
                                  {
                                    let ID;
                                    if(data.length===1) {
                                      ID = data[0]['_id']+1;
                                    } else {
                                      ID = 0;
                                    }
                                    DB.collection("stories").insert(
                                        {
                                          _id: ID,
                                          chain: SHA(datas['userID'] + ID + parseInt(Math.random()*1000000)),
                                          author: datas['userID'],
                                          genre: "story",
                                          title: datas['title'],
                                          content: datas['content'],
                                          actions: [],
                                          candidates: []
                                        }
                                        )
                                      .then(function(data2)
                                          {
                                            res.end(JSON.stringify(data2.ops[0]));
                                          })
                                    .catch(function(err)
                                        {
                                          console.error(err);
                                          res.sendStatus(500);
                                        });
                                  })
                            .catch(function(err)
                                {
                                  console.error(err);
                                  res.sendStatus(500);
                                });
                          } else {
                            res.sendStatus(500);
                          }
                        }
                        break;
                      case 'edit':
                        if(datas['storyref']) {
                          if(datas['content']) {
                            DB.collection("stories").find({_id: datas['storyref']}).toArray()
                              .then(function(data)
                                  {
                                    if(data.length===1) {
                                      const NUM = data[0]['actions'].length;
                                      let LAST = null;
                                      if(NUM>0) LAST = data[0]['actions'][NUM-1];
                                      let chain = datas['userID'] + NUM;
                                      if(LAST) chain += LAST['chain'];
                                      const OBJ = {
                                        number: NUM,
                                        chain: SHA(chain),
                                        author: datas['userID'],
                                        login: datas['userLOGIN'],
                                        title: datas['title'],
                                        content: datas['content'],
                                        action: datas['action']
                                      }
                                      DB.collection("stories").update(
                                          {_id: datas['storyref']},
                                          {$addToSet: {actions: OBJ}}
                                          )
                                        .then(function(data2)
                                            {
                                              res.end(JSON.stringify(OBJ));
                                            })
                                      .catch(function(err)
                                          {
                                            console.error(err);
                                            res.sendStatus(500);
                                          });
                                    } else {
                                      res.sendStatus(500);
                                    }
                                  })
                            .catch(function(err)
                                {
                                  console.error(err);
                                  res.sendStatus(500);
                                });
                          } else {
                            res.sendStatus(500);
                          }
                        } else {
                          res.sendStatus(500);
                        }
                        break;
                      case 'delete':
                        if(datas['storyref']) {
                          DB.collection("stories").find({_id: datas['storyref']}).toArray()
                            .then(function(data)
                                {
                                  if(data.length===1) {
                                    const NUM = data[0]['actions'].length;
                                    let LAST = null;
                                    if(NUM>0) LAST = data[0]['actions'][NUM-1];
                                    let chain = datas['userID'] + NUM;
                                    if(LAST) chain += LAST['chain'];
                                    const OBJ = {
                                      number: NUM,
                                      chain: SHA(chain),
                                      author: datas['userID'],
                                      login: datas['userLOGIN'],
                                      action: datas['action']
                                    }
                                    DB.collection("stories").update(
                                        {_id: datas['storyref']},
                                        {$addToSet: {actions: OBJ}}
                                        )
                                      .then(function(data2)
                                          {
                                            res.end(JSON.stringify(OBJ));
                                          })
                                    .catch(function(err)
                                        {
                                          console.error(err);
                                          res.sendStatus(500);
                                        });
                                  } else {
                                    res.sendStatus(500);
                                  }
                                })
                          .catch(function(err)
                              {
                                console.error(err);
                                res.sendStatus(500);
                              });
                        } else {
                          res.sendStatus(500);
                        }
                        break;
                      case 'end':
                        if(datas['storyref']) {
                          DB.collection("stories").find({_id: datas['storyref']}).toArray()
                            .then(function(data)
                                {
                                  if(data.length===1) {
                                    const NUM = data[0]['actions'].length;
                                    let LAST = null;
                                    if(NUM>0) LAST = data[0]['actions'][NUM-1];
                                    let chain = datas['userID'] + NUM;
                                    if(LAST) chain += LAST['chain'];
                                    const OBJ = {
                                      number: NUM,
                                      chain: SHA(chain),
                                      author: datas['userID'],
                                      login: datas['userLOGIN'],
                                      action: datas['action']
                                    }
                                    DB.collection("stories").update(
                                        {_id: datas['storyref']},
                                        {$set: {status: 'end'}, $addToSet: {actions: OBJ}}
                                        )
                                      .then(function(data2)
                                          {
                                            res.end(JSON.stringify(OBJ));
                                          })
                                    .catch(function(err)
                                        {
                                          console.error(err);
                                          res.sendStatus(500);
                                        });
                                  } else {
                                    res.sendStatus(500);
                                  }
                                })
                          .catch(function(err)
                              {
                                console.error(err);
                                res.sendStatus(500);
                              });
                        } else {
                          res.sendStatus(500);
                        }
                        break;
                      default:
                        res.sendStatus(500);
                        break;
                    }
                  } else {
                    res.sendStatus(403);
                  }
                } else {
                  res.sendStatus(403);
                }
              })
        .catch(function(err)
            {
              console.error(err);
              res.sendStatus(500);
            });
      } else {
        console.log("b");
        res.sendStatus(403);
      }
    });

//Last Stories
//GET: send last stories loaded from DB
//POST: create new story
  app.route("/pages/mainpage/stories/last")
.get(function(req, res)
    {
      DB.collection("stories").find().sort({_id: -1}).limit(10).toArray()
        .then(function(data)
            {
              res.end(JSON.stringify(treatStoriesData(data)));
            })
      .catch(function(err)
          {
            console.error(err);
            res.sendStatus(500);
          });
    });

//Random Stories
//GET: send stories loaded from DB
  app.route("/pages/mainpage/stories/random")
.get(function(req, res)
    {
      DB.collection("stories").aggregate(
          [ { $sample: { size: 10 } } ]
          ).toArray()
        .then(function(data)
            {
              res.end(JSON.stringify(treatStoriesData(data)));
            })
      .catch(function(err)
          {
            console.error(err);
            res.sendStatus(500);
          });
    });

//SearchUser
//GET: send user data
  app.route("/search/user")
.post(function(req, res)
    {
      const datas = JSON.parse(Object.keys(req.body)[0]);

      DB.collection("users").find({_id: datas['id']}).toArray()
        .then(function(data)
            {
              if(data.length===1) {
                const OBJ = {
                  id: data[0]['_id'],
                  authority: data[0]['authority'],
                  avatar: data[0]['avatar'],
                  login: data[0]['login']
                };
                res.end(JSON.stringify(OBJ));
              } else {
                res.sendStatus(500);
              }
            })
      .catch(function(err)
          {
            console.error(err);
            res.sendStatus(500);
          });
    });
