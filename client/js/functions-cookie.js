const writeCookie = function(mail, token)
{
  const date = new Date();
  date.setTime(date.getTime()+(365*24*60*60*1000));
  const expires = date.toGMTString();
  document.cookie = 'data='+mail+'|'+token+';expires='+expires+';path=/';
}
const readCookie = function()
{
  let tmp;
  let str = document.cookie;
  str = str.split(";");
  str = str.map(function(element, index, array)
      {
        return element.split("=");
      });
  str = str.map(function(element, index, array)
      {
        return element.map(function(element2, index2, array2)
        {
          return element2.trim();
        });
      });
  tmp = str.find(function(element, index, array)
      {
        return element[0]==="data";
      });
  if(tmp) {
    return true;
  } else {
    return false;
  }
}
