const canModify = function(field)
{
  const session1 = SELFSESSION;
  const session2 = USERSESSION;
  if(!session1 || !session2) return false;

  let cond = session1.getId()==session2.getId();
  switch(field) {
    case 'avatar':
      cond = cond
        ||
        session1.isAtLeast("Admin");
      break;
    case 'login':
      cond = cond
        ||
        session1.isAtLeast("Admin");
      break;
    case 'authority':
      cond = !cond
        &&
        session2.getAuthority()!="MasterAdmin"
        &&
        session1.isAtLeast("Admin")
        &&
        session1.isAtLeast(session2.getAuthority());
      break;
    case 'pass':
      cond = cond
        ||
        session1.isAtLeast("MasterAdmin");
      break;
    default:
      break;
  }
  return cond;
}

const validate_mail = function(str)
{
  let OBJ = {};
  const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(regex.test(str)) {
    OBJ.status = "OK";
  } else {
    OBJ.status = "KO";
    OBJ.details = [];
    OBJ.details.push("Mail invalide");
  }
  return OBJ;
}
const validate_pass = function(str)
{
  let OBJ = {};
  let length = str && str.length>=8;
  let upper = /[A-Z]/.test(str);
  let lower = /[a-z]/.test(str);
  let number = /\d/.test(str);
  if(length && upper && lower && number) {
    OBJ.status = "OK";
  } else {
    OBJ.status = "KO";
    OBJ.details = [];
    if(!length) OBJ.details.push("Mininum 8 caractères");
    if(!upper) OBJ.details.push("Doit contenir une majuscule");
    if(!lower) OBJ.details.push("Doit contenir une minuscule");
    if(!number) OBJ.details.push("Doit contenir un chiffre");
  }
  return OBJ;
}
const validate_image = function(image, bytes)
{
  let OBJ = {};
  let size = bytes<2500;
  let width = image.width>=50 && image.width<=200;
  let height = image.height>=50 && image.height<=200;
  let square = image.width===image.height;
  if(size && width && height && square) {
    OBJ.status = "OK";
  } else {
    OBJ.status = "KO";
    OBJ.details = [];
    if(!size) OBJ.details.push("L'image doit peser moins de 2.5ko");
    if(!width) OBJ.details.push("La largeur doit être entre 50px et 200px");
    if(!height) OBJ.details.push("La hauteur doit être entre 50px et 200px");
    if(!square) OBJ.details.push("L'image doit être carrée");
  }
  return OBJ;
}
const validate_login = function(str)
{
  let OBJ = {};
  let min = str.length>=3;
  let max = str.length<=30;
  if(min && max) {
    OBJ.status = "OK";
  } else {
    OBJ.status = "KO";
    OBJ.details = [];
    if(!length) OBJ.details.push("Le login doit être entre 3 et 30 caractères");
  }
  return OBJ;
}
const validate_form = function(id, obj, validate)
{
  let valid = true;
  if(validate) {
    $('#'+id+'-msg').remove();
    $('#'+id).find('.help').remove();
    $('#'+id).find('input').removeClass("invalid");

    const keys = Object.keys(obj);
    keys.forEach(function(element, index, array)
        {
          const func = eval('validate_'+element);
          const vals = [];
          obj[element].forEach(function(element2, index2, array2)
              {
                vals.push($('#'+id).find('input[name="'+element2+'"]').val());
              });
          const VALIDITY = func(vals[0]);
          if('details' in VALIDITY) {
            const help = jQuery('<span></span>').addClass('help').attr('name', id).html(VALIDITY.details.join('<br>'));
            $('#'+id).find('input[name="'+obj[element][0]+'"]').closest('.input').append(help);
            obj[element].forEach(function(element2, index2, array2)
                {
                  $('#'+id).find('input[name="'+element2+'"]').addClass("invalid");
                });
            valid = false;
          }
          const equality = vals.reduce(function(a, b)
              {
                return (a===b) ? a : NaN;
              });
          if(equality!=vals[0]) {
            if($('#'+id+'-msg').length===0) {
              const msg = jQuery('<p></p>').addClass('msg').attr('id', id+'-msg');
              $('#'+id).prepend(msg);
            }
            $('#'+id+'-msg').append("Valeurs différentes").show();
            obj[element].forEach(function(element2, index2, array2)
                {
                  $('#'+id).find('input[name="'+element2+'"]').addClass("invalid");
                });
            valid = false;
          }
        });
  }
  return valid;
}
const getSignFormData = function(id, obj)
{
  const OBJ = {
    action: id,
  };
  const keys = Object.keys(obj);

  switch(id) {
    case 'signformup':
    case 'signformin':
      OBJ['cookie'] = $('#'+id).find('input[name="cookie"]').is(':checked');
    case 'signformresetpass':
      keys.forEach(function(element, index, array)
          {
            obj[element].forEach(function(element2, index2, array2)
                {
                  OBJ[element2] = $('#'+id).find('input[name="'+element2+'"]').val();
                });
          });
      break;
    default:
      break;
  }
  if(id==='signformresetpass') OBJ["newpass"] = generatePassword();

  return JSON.stringify(OBJ);
}
const getChangeFormData = function(id, obj)
{
  const OBJ = {
    action: id,
    selfID: SELFSESSION.getId(),
    selfAUTHORITY: SELFSESSION.getAuthority(),
    userID: USERSESSION.getId(),
    userAUTHORITY: USERSESSION.getAuthority(),
  };
  const keys = Object.keys(obj);

  switch(id) {
    case 'avatarchangeform':
      OBJ['avatar'] = $('#avatarchangeform').find('input[name="avatar"]').prop('files')[0];
      OBJ['avatarimg'] = $('#avatarchangeform').find('#newavatar').wrapAll('<div>').parent().html();
      break;
    case 'authoritychangeform':
      OBJ['authority'] = $('#authoritychangeform').find('select option:selected').attr('name');
      break;
    case 'loginchangeform':
    case 'passchangeform':
      keys.forEach(function(element, index, array)
          {
            obj[element].forEach(function(element2, index2, array2)
                {
                  OBJ[element2] = $('#'+id).find('input[name="'+element2+'"]').val();
                });
          });
      break;
    default:
      break;
  }

  return JSON.stringify(OBJ);
}
const getSignMessage = function(id)
{
  switch(id) {
    case 'signformin':
      return "Utilisateur ou Mot de Passe incorrect";
      break;
    case 'signformup':
      return "Utilisateur Existant";
      break;
    case 'signformresetpass':
      return "Utilisateur Inexistant";
      break;
    default:
      break;
  }
}
const getValidChangeMessage = function(id)
{
  switch(id) {
    case 'avatarchangeform':
      return "Nouvel Avatar";
      break;
    case 'authoritychangeform':
      return "Nouvelle Autorité";
      break;
    case 'loginchangeform':
      return "Nouveeau Login";
      break;
    case 'passchangeform':
      return "Nouveeau Mot de Passe";
      break;
    default:
      break;
  }
}
const getValidChangeData = function(id, obj)
{
  switch(id) {
    case 'avatarchangeform':
      return obj["avatarimg"];
      break;
    case 'authoritychangeform':
      return obj["authority"];
      break;
    case 'loginchangeform':
      return obj["login1"];
      break;
    case 'passchangeform':
      return "********";
      break;
    default:
      break;
  }
}

const submitSignForm = function(id, obj)
{
  const FIELDS = [
    "signformup",
    "signformin",
    "signformresetpass"];
  const noValidate = [];
  const toLog = [
    "signformin",
    "signformup"];

  $.ajax({
    url: id.replace("form","/"),
    method: 'POST',
    data: getSignFormData(id, obj),
    beforeSend: function(jqXHR, settings)
    {
      jqXHR.setRequestHeader('from-within', 'from-within');
      const datas = JSON.parse(settings.data);

      if(!validate_form(id, obj, !noValidate.includes(id))) {
        jqXHR.abort();
      }
    },
    complete: function(jqXHR, textStatus){},
    error: function(jqXHR, textStatus, errorThrown)
    {
      console.error(errorThrown);
      const p = jQuery('<p></p>').attr('id', id+'-msg').addClass('delete');
      p.html(getSignMessage(id));
      $('#'+id).prepend(p);
    },
    success: function(data, textStatus, jqXHR)
    {
      $('.formblockholder').hide();
      if(id==='signformresetpass') $('#signblockin').show();
      if(toLog.includes(id)) logUser(JSON.parse(data));
    }
  });
}
const submitChangeForm = function(id, obj)
{
  const FIELDS = [
    "avatarchangeform",
    "authoritychangeform",
    "loginchangeform",
    "passchangeform"]
      const noValidate = [
      "avatarchangeform",
      "authoritychangeform"];

  $.ajax({
    url: 'change/'+id.substr(0, id.length - "changeform".length),
    method: 'POST',
    data: getChangeFormData(id, obj),
    beforeSend: function(jqXHR, settings)
    {
      jqXHR.setRequestHeader('from-within', 'from-within');
      const datas = JSON.parse(settings.data);

      console.log(datas);

      if(validate_form(id, obj, !noValidate.includes(id))) {
        const p = jQuery('<p></p>').attr('id', id+'-valid').addClass('formvalid');
        const strong = jQuery('<strong></strong>').html(getValidChangeMessage(id));
        const span = jQuery('<span></span>').addClass('newvalue').html(getValidChangeData(id, datas));
        p.append(strong);
        p.append('<br>');
        p.append(span);
        $('#'+id).removeClass("displayed");
        $('#'+id).after(p);
      } else {
        jqXHR.abort();
      }
    },
    complete: function(jqXHR, textStatus){},
    error: function(jqXHR, textStatus, errorThrown)
    {
      console.error(errorThrown);
    },
    success: function(data, textStatus, jqXHR)
    {
      if(data) {
        switch(id) {
          case 'avatarchangeform':
            $('#avatar').attr('src', $('#'+id+'-valid').children('.newvalue').children('img').attr('src'));
            $('#avatarinfo').attr('src', $('#'+id+'-valid').children('.newvalue').children('img').attr('src'));
            break;
          case 'loginchangeform':
            $('#login').html($('#'+id+'-valid').children('.newvalue').html());
            $('#logininfo').html($('#'+id+'-valid').children('.newvalue').html());
            break;
          case 'authoritychangeform':
            $('#authorityinfo').html($('#'+id+'-valid').children('.newvalue').html());
            break;
          case 'passchangeform':
            break;
          default:
            break;
        }
        $('#'+id+'-valid').remove();
      } else {
        $('#'+id+'-valid').addClass('formerror').removeClass('formvalid');
      }
    }
  });
}
