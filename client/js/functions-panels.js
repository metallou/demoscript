"use strict";

const initChangeButtons = function()
{
  const FIELDS = ["authority", "avatar", "login", "pass"];
  FIELDS.forEach(function(element, index, array)
      {
        if(canModify(element)) {
          $("#"+element+"change").removeClass("hidden");
        } else {
          $("#"+element+"change").addClass("hidden");
        }

        $("#"+element+"change").on("click", function()
            {
              if(canModify(element)) {
                getInfoChange("change/"+element, element+"change");
              }
            });
      });
}
const initChangeAuthority = function()
{
  const select = $('#authoritychangeform').find('select').empty();
  const AUTHORITIES1 = ["User", "Moderator"];
  AUTHORITIES1.forEach(function(element, index, array)
      {
        const option = jQuery('<option></option>').attr('name', element).html(element.toUpperCase());
        select.append(option);
      });
  const AUTHORITIES2 = ["Admin"];
  AUTHORITIES2.forEach(function(element, index, array)
      {
        const option = jQuery('<option></option>').attr('name', element).html(element.toUpperCase());
        if(SELFSESSION.isAtLeast(element)) select.append(option);
      });
}

const displayUserPanel = function()
{
  const displaySessionData = function()
  {
    $("#infoblock").find("#avatarinfo").find("img").prop('src', USERSESSION.getAvatar());
    $("#infoblock").find("#logininfo").html(USERSESSION.getLogin());
    $("#infoblock").find("#authorityinfo").html(USERSESSION.getAuthority());
    initChangeButtons();
  }

  if($("#userinfoblock").length===0) {
    $.ajax({
      url: 'pages/userinfo',
      method: 'GET',
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown)
      {
        console.error(errorThrown);
      },
      success: function(data, textStatus, jqXHR)
      {
        $("#FEEDME").html(data);

        displaySessionData();
      }
    });
  } else {
    $("#userinfoblock").show();

    displaySessionData();
  }
}
const getUserPanel = function(id)
{
  if(SELFSESSION.getId()===id) {
    USERSESSION = SELFSESSION;
    displayUserPanel();
  } else {
    $.ajax({
      url: "search/user",
      method: "POST",
      data: JSON.stringify({id: id}),
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown)
      {
        console.error(errorThrown);
      },
      success: function(data, textStatus, jqXHR)
      {
        USERSESSION = new Session(JSON.parse(data));
        displayUserPanel();
      }
    });
  }
}
const getInfoChange = function(url, id)
{
  const resetform = function(id)
  {
    $('#'+id+'form-valid').remove();
    $('#'+id+'form').find('img').remove();
    $('#'+id+'form').find('input').not(':button, :hidden').val("");
    $('#'+id+'form').addClass("displayed");
    if(id==="authoritychange") initChangeAuthority();
  }

  if($('#'+id+'form').length===0) {
    //GET form
    $.ajax({
      url: url,
      method: "GET",
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown)
      {
        console.error(errorThrown);
      },
      success: function(data, textStatus, jqXHR)
      {
        $('#'+id).closest("section").append(data);
        resetform(id);
      }
    });
  } else {
    resetform(id);
  }
}
const getLoginButtons = function()
{
  if($("#loggedpanel").length===1) $("#loggedpanel").hide();
  if($("#logpanel").length===0) {
    $.ajax({
      url: 'log',
      method: 'GET',
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown)
      {
        console.error(errorThrown);
      },
      success: function(data, textStatus, jqXHR)
      {
        $("#userblock").append(data);
      }
    });
  } else {
    $("#logpanel").show();
  }
}
const getMainPage = function(sort)
{
  const displayData = function(sort)
  {
    loadStories(sort);
  }

  $(".feedme").hide();
  if($("#mainpage").length===0) {
      $.ajax({
        url: 'pages/mainpage',
        method: 'GET',
        beforeSend: function(jqXHR, settings)
        {
          jqXHR.setRequestHeader('from-within', 'from-within');
        },
        complete: function(jqXHR, textStatus){},
        error: function(jqXHR, textStatus, errorThrown)
        {
          console.error(errorThrown);
        },
        success: function(data, textStatus, jqXHR)
        {
          $("#FEEDME").html(data);
          displayData(sort);
        }
      });
  } else {
    $("#mainpage").show();
    displayData(sort);
  }
}
