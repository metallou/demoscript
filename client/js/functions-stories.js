const buildOwnerList = function(ownerblock, data)
{
  console.log(data);
  let li;
  const ul = ownerblock.children('ul');
  ul.empty();

  const svg = '<svg height="30px" width="30px">'
    +'<line x1="5" x2="25" y1="15" y2="15" />'
    +'</svg>';
  li = jQuery('<li></li>').addClass('close').html(svg);
  ul.append(li);
  li.on('click', function()
      {
        removeOwnerList($(this).closest('.storycommands'), data);
      });
  li = jQuery('<li></li>').html(data['login']);
  ul.append(li);
  li.on('click', function()
      {
        getUserPanel(data['author']);
      });
  ownerblock.addClass('active');
}
const removeOwnerList = function(ownerblock, data)
{
  let li;
  const ul = ownerblock.children('ul');
  ul.empty();

  const svg = '<svg height="30px" width="30px">'
    +'<line x1="5" x2="25" y1="15" y2="15" />'
    +'<line y1="5" y2="25" x1="15" x2="15" />'
    +'</svg>';
  li = jQuery('<li></li>').addClass('open').html(svg);
  ul.append(li);
  li.on('click', function()
      {
        buildOwnerList($(this).closest('.storycommands'), data);
      });
  ownerblock.removeClass('active');
}
const buildOwnerBlock = function(data)
{
  const ownerblock = jQuery('<div></div>').addClass('storycommands');
  const ul = jQuery('<ul></ul>');
  ownerblock.append(ul);
  removeOwnerList(ownerblock, data);

  return ownerblock;
}
const addMark = function(button, tag)
{
  const content = $(button).closest('.storyform').find('.storyformcontent');
  const selectionStart = content.prop('selectionStart');
  const selectionEnd = content.prop('selectionEnd');

  let startTag = '';
  let endTag = '';
  switch(tag)
  {
    case "bold":
      startTag = '_B';
      endTag = 'B_';
      break;
    case "italic":
      startTag = '_I';
      endTag = 'I_';
      break;
    case "underline":
      startTag = '_U';
      endTag = 'U_';
      break;
    case "size":
      startTag = '_S(16px)';
      endTag = '|S_';
      break;
    case "color":
      startTag: '_C(rgb(255,223,0))';
      endTag: '|C_'
    default:
        break;
  }

  let tmp = '';
  const str = content.val();
  tmp += str.substring(0, selectionStart);
  tmp += startTag;
  tmp += str.substring(selectionStart, selectionEnd);
  tmp += endTag;
  tmp += str.substring(selectionEnd);

  content.val(tmp);
}
const tagifyContent = function(str)
{
  //insert funcking every fucking other fucking HTML fucking special fucking characters
  let tmp = '\t'+str;

  const TAGS = [
  {
    in: 'B',
    start: '<b>',
    end: '</b>'
  },
  {
    in: 'I',
    start: '<i>',
    end: '</i>'
  },
  {
    in: 'U',
    start: '<u>',
    end: '</u>'
  },
  {
    in: 'S',
    start: '<del>',
    end: '</del>'
  },
  {
    in: 'M',
    start: '<mark>',
    end: '</mark>'
  },
  {
    in: 'SUB',
    start: '<sub>',
    end: '</sub>'
  },
  {
    in: 'SUP',
    start: '<sup>',
    end: '</sup>'
  }];
  TAGS.forEach(function(element, index, array)
      {
        tmp = tmp.replace('_'+element.in, element.start);
        tmp = tmp.replace(element.in+'_', element.end);
      });

  const VARS = [
  {
    in: 'S',
    out: 'font-size:'
  },
  {
    in: 'C',
    out: 'color:'
  }
  ];
  VARS.forEach(function(element, index, array)
      {
        tmp = tmp.replace('_'+element.in+'(', '<span style="'+element.out);
        tmp = tmp.replace(')'+element.in, '">');
        tmp = tmp.replace('|'+element.in+'_', '</span>');
      });

  const CHARS = [
  {
    in: '\n',
    out: '<br>'
  },
  {
    in: '\t',
    out: '&#09'
  },
  {
    in: '{{TRADE}}',
    out: '&trade;'
  }];
  CHARS.forEach(function(element, index, array)
      {
        tmp = tmp.replace(element.in, element.out);
      });

  return tmp;
}
const buildAction = function(block, append, data)
{
  let content;
  let title;
  const action = jQuery('<section></section>').addClass('storyblock');
  const span = jQuery('<span></span>').html(data['number']).hide();
  action.append(span);
  switch(data['action']) {
    case 'edit':
    case 'new':
      if(element['title']) {
        title = jQuery('<h2></h2>').addClass('storytitle').html(data['title']);
        action.append(title);
      }
      content = jQuery('<p></p>').addClass('storycontent').html(tagifyContent(data['content']));
      action.append(content);
      break;
    case 'delete':
      content = jQuery('<p></p>').addClass('delete').html('DELETE');
      action.append(content);
      break;
    case 'end':
      content = jQuery('<p></p>').addClass('end').html('END');
      action.append(content);
      break;
    default:
      break;
  }
  action.append(buildOwnerBlock(data));
  block.append(action);
}
const buildStory = function(block, append, data)
{
  let actions = [];
  let candidates = [];

  const article = jQuery('<article></article>').addClass('story');
  if(append) {
    block.append(article);
  } else {
    block.prepend(article);
  }
  const span = jQuery('<span></span>').html(data['_id']).hide();
  article.append(span);
  const section = jQuery('<section></section>').addClass('storyblock storyblockmain');
  article.append(section);

  let title = jQuery('<h1></h1>').addClass('storytitle storytitlemain').html(data['title']);
  section.append(title);
  let content = jQuery('<p></p>').addClass('storycontent storycontentmain').html(tagifyContent(data['content']));
  section.append(content);
  section.append(buildOwnerBlock(data));

  const svg = '<svg width="50px" height="50px">'
    +'<path d="M25,45 L15,35 A 15 15 0 1 1 35,35 Z" fill="rgb(42,42,42)" stroke="none" />'
    +'<circle cx="25" cy="24" r="12" fill="rgb(255,223,0)" stroke="none" />'
    +'<path class="cross" d="M15,22 L23,22 L23,14 L27,14 L27,22 L35,22 L35,26 L27,26 L27,34 L23,34 L23,26 L15,26 Z" fill="rgb(42,42,42)" stroke="none" />'
    +'</svg>';
  const more = jQuery('<div></div>').addClass('storyreveal').html(svg);
  article.append(more);
  more.on('click', function()
      {
        $(this).closest('.story').addClass('active');
        $(this).remove();
      });

  const blockactions = jQuery('<div></div>').addClass('storyactions');
  article.append(blockactions);
  data['actions'].forEach(function(element, index, array)
      {
        let newaction = true;
        const action = jQuery('<section></section>').addClass('storyblock');
        const span = jQuery('<span></span>').html(element['number']).hide();
        action.append(span);
        switch(element['action']) {
          case 'edit':
            actions.pop();
          case 'new':
            if(element['title']) {
              title = jQuery('<h2></h2>').addClass('storytitle').html(element['title']);
              action.append(title);
            }
            content = jQuery('<p></p>').addClass('storycontent').html(tagifyContent(element['content']));
            action.append(content);
            break;
          case 'delete':
            actions.pop();
            newaction = false;
            break;
          case 'end':
            content = jQuery('<p></p>').addClass('end').html('END');
            action.append(content);
            break;
          default:
            break;
        }
        if(newaction) {
          action.append(buildOwnerBlock(element));
          actions.push(action);
        }
      });
  actions.forEach(function(element, index2, array2)
      {
        blockactions.append(element);
      });

  const buttons = [
  {
    text: 'new',
    action: function(){newCandidate('new', article, data['_id'], null);}
  },
  {
    text: 'edit',
    action: function(){newCandidate('edit', article, data['_id'], null);}
  },
  {
    text: 'delete',
    action: function(){newCandidate('delete', article, data['_id'], null);}
  },
  {
    text: 'end',
    action: function(){newCandidate('end', article, data['_id'], null);}
  }
  ];
  const toolbar = jQuery('<div></div>').addClass('storytoolbar');
  article.append(toolbar);
  const ul = jQuery('<ul></ul>');
  toolbar.append(ul);
  buttons.forEach(function(element, index, array)
      {
        const li = jQuery('<li></li>');
        ul.append(li);
        const button = jQuery('<button></button>').addClass('storybutton').addClass(element.text).html(element.text);
        button.on('click', function()
            {
              element.action();
            });
        li.append(button);
      });

  const blockcandidates = jQuery('<div></div>').addClass('storycandidates');
  article.append(blockcandidates);
  data['candidates'].forEach(function(element, index, array)
      {
        const candidate = jQuery('<section></section>').addClass('storyblock storycandidate');
        switch(element['action']) {
          case 'new':
            candidate.addClass("new");
            if(element['title']) {
              title = jQuery('<h2></h2>').addClass('storytitle').html(element['title']);
              candidate.append(title);
            }
            content = jQuery('<p></p>').addClass('storycontent').html(tagifyContent(element['content']));
            candidate.append(content);
            break;
          case 'edit':
            candidate.addClass("edit");
            if(element['title']) {
              title = jQuery('<h2></h2>').addClass('storytitle').html(element['title']);
              candidate.append(title);
            }
            content = jQuery('<p></p>').addClass('storycontent').html(tagifyContent(element['content']));
            candidate.append(content);
            break;
          case 'delete':
            candidate.addClass("delete");
            content = jQuery('<p></p>').html("DELETE");
            candidate.append(content);
            break;
          case 'end':
            candidate.addClass("end");
            content = jQuery('<p></p>').html("END");
            candidate.append(content);
            break;
          default:
            break;
        }
        candidate.append(buildOwnerBlock(element));
        candidates.push(candidate);
      });
  candidates.forEach(function(element, index, array)
      {
        blockcandidates.append(element);
      });
}
const buildStories = function(data)
{
  const block = jQuery('<div></div>').attr('id', 'stories');
  $("#mainpage").append(block);
  JSON.parse(data).forEach(function(element, index, array)
      {
        buildStory(block, true, element);
      });
}
const createStory = function()
{
  const resetForm = function(show)
  {
    resetCheck($('#newstory'));
    $('#newstory').find('input[name="action"]').val('new');
    $('#newstory').find('input[name="storyref"]').val('');
    $('#newstory').find('[name="blockref"]').val('');
    $('#newstory').find('input').not(':button, :hidden, :submit').val('');
    if(show) {
      $('#newstory').show();
    } else {
      $('#newstory').hide();
    }
  };

  if($('#newstory').length===0) {
    $.ajax({
      url: 'pages/mainpage/stories/new',
      method: 'GET',
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown)
      {
        console.error(errorThrown);
      },
      success: function(data, textStatus, jqXHR)
      {
        const shade = jQuery('<div></div>').addClass('shade');
        shade.on('click', function()
            {
              resetForm(false);
            });
        const form = jQuery('<div></div>').addClass('formblock').html(data);
        const formholder = jQuery('<div></div>').attr('id', 'newstory').addClass('formblockholder').append(shade).append(form);
        $(document.body).append(formholder);
        resetForm(true);
      }
    });
  } else {
    resetForm(true);
  }
}
const newCandidate = function(action, block, storyref, blockref)
{
  const noForm = ['delete', 'end'];
  const resetForm = function(candidate, show)
  {
    resetCheck(candidate);
    $(candidate).find('input').not(':button, :hidden, :submit').val('');
    $(candidate).find('[name="action"]').val(action);
    $(candidate).find('[name="storyref"]').val(storyref);
    $(candidate).find('[name="blockref"]').val(blockref);
    if(show) {
      $(candidate).show();
    } else {
      $(candidate).hide();
    }
    if(noForm.includes(action)) submitBlock(candidate.find('[name="action"]'), false);
  };

  if($(block).find('.newcandidate').length===0) {
    $.ajax({
      url: 'pages/mainpage/stories/new',
      method: 'GET',
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown)
      {
        console.error(errorThrown);
      },
      success: function(data, textStatus, jqXHR)
      {
        const candidate = jQuery('<div></div>').addClass('newcandidate').html(data);;
        $(block).append(candidate);
        if(noForm.includes(action)) {
          resetForm($(block).find('.newcandidate'), false);
        } else {
          resetForm($(block).find('.newcandidate'), true);
        }
      }
    });
  } else {
    if(noForm.includes(action)) {
      resetForm($(block).find('.newcandidate'), false);
    } else {
      resetForm($(block).find('.newcandidate'), true);
    }
  }
}
const loadStories = function(sort)
{
  $("#mainpage").empty();
  loadMoreStories(sort, 0);
}
const loadMoreStories = function(sort, skip)
{
  let sorturl = "last";
  switch(sort)
  {
    case "random":
    case "last":
      sorturl = sort;
      break;
    default:
      sorturl = "last";
      break;
  }

  $.ajax({
    url: 'pages/mainpage/stories/'+sorturl,
    method: 'GET',
    beforeSend: function(jqXHR, settings)
    {
      jqXHR.setRequestHeader('from-within', 'from-within');
    },
    complete: function(jqXHR, textStatus){},
    error: function(jqXHR, textStatus, errorThrown)
    {
      console.error(errorThrown);
    },
    success: function(data, textStatus, jqXHR)
    {
      buildStories(data);
    }
  });
}

const previewBlock = function(button)
{
  const parent = $(button).closest('.storyform');

  parent.find('.storyformpreview').removeClass('preview').empty();
  const title = parent.find('.storyformtitle').val();
  const str = parent.find('.storyformcontent').val();
  const tmp = tagifyContent(str);

  let h = "";
  if(title!="") h = jQuery('<h1></h1>').addClass('storytitle').html(title);
  const p = jQuery('<p></p>').addClass('storycontent').html(tmp);
  parent.find('.storyformpreview').addClass('preview').append(h).append(p);
}
const resetCheck = function(story)
{
  story.find('[name="title"]').removeClass('invalid');
  story.find('[name="content"]').removeClass('invalid');
}
const checkStory = function(story)
{
  const OBJ = {
    action: story.find('[name="action"]').val(),
    storyref: story.find('[name="storyref"]').val(),
    blockref: story.find('[name="blockref"]').val(),
    title: story.find('[name="title"]').val(),
    content: story.find('[name="content"]').val()
  };

  const yesForm = ['new', 'edit'];
  const references = ['edit', 'delete', 'end'];

  const isEmpty = function(str)
  {
    //if(str==null || str==undefined || str=='') return true;
    let tmp = str;
    tmp = tmp.replace('\t', '');
    tmp = tmp.replace('\n', '');
    tmp = tmp.replace('\r', '');
    tmp = tmp.replace('\0', '');
    tmp = tmp.replace(' ', '');
    if(tmp=='') return true;
    return false;
  }

  resetCheck(story);

  if(isEmpty(OBJ.action)) return false;

  if(yesForm.includes(OBJ.action)) {
    if(isEmpty(OBJ.content)) {
      story.find('[name="content"]').addClass('invalid');
      return false;
    }
  }
  if(references.includes(OBJ.action)) {
    if(isEmpty(OBJ.storyref)) return false;
  } else {
    if(isEmpty(OBJ.storyref)) {
      OBJ.storyref = null;
      OBJ.blockref = null;
      if(isEmpty(OBJ.title)) {
        story.find('[name="title"]').addClass('invalid');
        return false;
      }
    } else {
      if(isEmpty(OBJ.blockref)) OBJ.blockref = null;
    }
  }

  if(isEmpty(OBJ.title)) OBJ.title = null;

  if(SELFSESSION) {
    OBJ.userID = SELFSESSION.getId();
    OBJ.userLOGIN = SELFSESSION.getLogin();
    OBJ.userAUTHORITY = SELFSESSION.getAuthority();
  } else {
    alert("Veuillez vous connecter");
    return false;
  }

  OBJ.storyref = parseInt(OBJ.storyref);
  OBJ.blockref = parseInt(OBJ.blockref);
  return JSON.stringify(OBJ);
}
const submitBlock = function(button, story)
{
  $.ajax({
    url: 'pages/mainpage/stories/new',
    method: 'POST',
    data: checkStory($(button).closest('.storyform')),
    beforeSend: function(jqXHR, settings)
    {
      if(settings.data) {
        jqXHR.setRequestHeader('from-within', 'from-within');
      } else {
        jqXHR.abort();
      }
    },
    complete: function(jqXHR, textStatus){},
    error: function(jqXHR, textStatus, errorThrown)
    {
      console.error(errorThrown);
    },
    success: function(data, textStatus, jqXHR)
    {
      if(story) {
        buildStory($('#stories'), false, JSON.parse(data));
      } else {
        const block = $(button).closest(".newcandidate").parent().find('.storyactions');
        buildAction(block, true, JSON.parse(data));
      }
      cancelBlock(button);
    }
  });
}
const cancelBlock = function(button)
{
  const parent = $(button).closest('.storyform');
  if(parent.parent().hasClass('formblock')) {
    $('#newstory').hide();
  } else {
    parent.hide();
  }
}
