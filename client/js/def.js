"use strict"

let SELFSESSION = null;
let USERSESSION = null;
const TYPES = {
  Boolean: 1,
  Number: 2,
  String: 3,
  Array: 4,
  Object: 5,
  Function: 6
};
const KEYS = {
  id: 1,
  authority: 2,
  login: 3,
  avatar: 4,
};
const AUTHORITIES = {
  User: 1,
  Moderator: 2,
  Admin: 3,
  MasterAdmin: 4,
};

class Check
{
  static def(object)
  {
    if(object===null) throw Error("null");
    if(object===undefined) throw Error("undefined");
  }
  static list(object, list)
  {
    let str;
    let comp;

    this.def(list);
    str = Object.prototype.toString.call(list);
    comp = Object.prototype.toString.call({});
    if(!(str===comp)) throw Error("wrong type"
        +"\n"
        +"(received " + str + ")"
        +"\n"
        +"(expected " + comp + ")");
    if(Object.keys(list).length===0) throw Error("empty set");

    this.def(object);
    str = Object.prototype.toString.call(object);
    comp = Object.prototype.toString.call("");
    if(str!=comp) throw Error("wrong type"
        +"\n"
        +"(received " + str + ")"
        +"\n"
        +"(expected " + comp + ")");
    if(!list[object]) throw Error("not in list"
        +"\n"
        +"(received \"" + object + "\")");
  }
  static proto(object, type)
  {
    this.list(type, TYPES);

    this.def(object);
    const str = Object.prototype.toString.call(object);
    const comp = "[object "+type+"]";
    if(str!=comp) throw Error("wrong type"
        +"\n"
        +"(received " + str + ")"
        +"\n"
        +"(expected " + "[object "+type+"]" + ")");
  }
  static sup(number, limit)
  {
    this.number(number);
    this.number(limit);
    if(number<limit) throw Error("too low"
        +"\n"
        +"(received "+number+"<"+limit+")");
  }
  static inf(number, limit)
  {
    this.number(number);
    this.number(limit);
    if(number>limit) throw Error("too low"
        +"\n"
        +"(received "+number+">"+limit+")");
  }
  static esup(number, limit)
  {
    this.number(number);
    this.number(limit);
    if(number<=limit) throw Error("too low"
        +"\n"
        +"(received "+number+"<="+limit+")");
  }
  static einf(number, limit)
  {
    this.number(number);
    this.number(limit);
    if(number>=limit) throw Error("too low"
        +"\n"
        +"(received "+number+">="+limit+")");
  }
  static eq(number, limit)
  {
    this.number(number);
    this.number(limit);
    if(number!=limit) throw Error("not equal"
        +"\n"
        +"(received "+number+"!="+limit+")");
  }
  static neq(number, limit)
  {
    this.number(number);
    this.number(limit);
    if(number===limit) throw Error("equal"
        +"\n"
        +"(received "+number+")");
  }





  static boolean(prout)
  {
    Check.def(prout);
    Check.proto(prout, "Boolean");
  }
  static number(prout)
  {
    Check.def(prout);
    Check.proto(prout, "Number");
  }
  static string(prout)
  {
    Check.def(prout);
    Check.proto(prout, "String");
  }
  static array(prout)
  {
    Check.def(prout);
    Check.proto(prout, "Array");
  }
  static object(prout)
  {
    Check.def(prout);
    Check.proto(prout, "Object");
  }
  static func(prout)
  {
    Check.def(prout);
    Check.proto(prout, "Function");
  }
}

const symbol3eae270732af4c664242eb539087137ef04001b423c5948c7a39d028ed892488 = Symbol();
const symbol12a867c1352a7bc1f205b0c8c3f131d5a69929772dcf3f0487d5572d64a81c9a = Symbol();
const symbolac15f316477dc674c7df09f7205418e05ca35e4a5ab83716eb11d4fd0a8e392c = Symbol();
const symbol2ceaa94cbab4c2c7c87fc963693a25a62ad1d20b1303eb5de940b0031f47fbf0 = Symbol();
