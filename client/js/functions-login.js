const logUserFromCookie = function(extData)
{
  $.ajax({
    url: 'log',
    method: 'POST',
    beforeSend: function(jqXHR, settings)
    {
      jqXHR.setRequestHeader('from-within', 'from-within');
    },
    complete: function(jqXHR, textStatus){},
    error: function(jqXHR, textStatus, errorThrown){console.error(errorThrown);},
    success: function(data, textStatus, jqXHR)
    {
      const intel = JSON.parse(data);
      if(intel["userFound"]) {
        logUser(intel.USER);
      } else {
        getLoginButtons();
      }
    }
  });
}
const logUser = function(user)
{
  SELFSESSION = new Session(user);
  $('#loginfade').hide();
  $('.loginblock').hide();

  if(user["mail"]) {
    $.ajax({
      url: 'logged',
      method: 'POST',
      data: JSON.stringify({
        mail: user["mail"],
        id: SELFSESSION.getId()
      }),
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown){},
      success: function(data, textStatus, jqXHR)
      {
        writeCookie(user["mail"], data);
      }
    });
  }

  $("#logpanel").hide();
  if($("#loggedpanel").length===0) {
    $.ajax({
      url: 'logged',
      method: 'GET',
      beforeSend: function(jqXHR, settings)
      {
        jqXHR.setRequestHeader('from-within', 'from-within');
      },
      complete: function(jqXHR, textStatus){},
      error: function(jqXHR, textStatus, errorThrown){},
      success: function(data, textStatus, jqXHR)
      {
        $('#userblock').append(data);
        $('#login').html(SELFSESSION.getLogin());
        $('#avatar').attr('src', SELFSESSION.getAvatar());
      }
    });
  } else {
    $("#loggedpanel").show();
    $('#login').html(SELFSESSION.getLogin());
    $('#avatar').attr('src', SELFSESSION.getAvatar());
  }
}
const generatePassword = function()
{
  const chars = [
  ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
  ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
  ["0","1","2","3","4","5","6","7","8","9"]
  ];

  let str;
  let PASS;
  let inputs = [];
  let index;
  do {
    inputs = [];
    for(let i=0; i<(8+(parseInt(Math.random()*1000*10)%10)); i++) {
      index = parseInt(Math.random()*1000*chars.length)%chars.length;
      inputs[i] = chars[index][parseInt(Math.random()*1000*chars[index].length)%chars[index].length];
    }
    str = inputs.join("");
    PASS = validate_pass(str);
  } while('details' in PASS);
  return str;
}
