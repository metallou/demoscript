"use strict";

class Session
{
  constructor(obj)
  {
    Check.proto(obj, "Object");
    const keys = Object.keys(obj);
    let invalid = false;
    if(!keys.includes("id")) {
      invalid = true;
      console.log("a");
    } else {
      Check.number(obj["id"]);
      Check.sup(obj["id"], 0);
    }
    if(!keys.includes("authority")) {
      invalid = true;
      console.log("b");
    } else {
      Check.string(obj["authority"]);
      Check.list(obj["authority"], AUTHORITIES);
    }
    if(!keys.includes("login")) {
      invalid = true;
      console.log("c");
    } else {
      Check.string(obj["login"]);
    }
    if(!keys.includes("avatar")) {
      invalid = true;
      console.log("d");
    } else {
      Check.string(obj["avatar"]);
      let tmp = obj["avatar"].split("/");
      if(tmp.length!=4) {
        invalid = true;
        console.log(tmp.length);
        tmp[3] = tmp[3].split(".");
        if(tmp[3].length!=2) invalid = true;
        if(tmp[3][1]!="png") invalid = true;
      }
    }

    if(invalid) {
      throw Error("invalid object");
    } else {
      this[symbol3eae270732af4c664242eb539087137ef04001b423c5948c7a39d028ed892488] = obj["id"];
      this[symbol12a867c1352a7bc1f205b0c8c3f131d5a69929772dcf3f0487d5572d64a81c9a] = obj["authority"];
      this[symbolac15f316477dc674c7df09f7205418e05ca35e4a5ab83716eb11d4fd0a8e392c] = obj["login"];
      this[symbol2ceaa94cbab4c2c7c87fc963693a25a62ad1d20b1303eb5de940b0031f47fbf0] = obj["avatar"];
    }
  }
  getId()
  {
    return this[symbol3eae270732af4c664242eb539087137ef04001b423c5948c7a39d028ed892488];
  }
  getLogin()
  {
    return this[symbolac15f316477dc674c7df09f7205418e05ca35e4a5ab83716eb11d4fd0a8e392c];
  }
  getAvatar()
  {
    return this[symbol2ceaa94cbab4c2c7c87fc963693a25a62ad1d20b1303eb5de940b0031f47fbf0];
  }
  getAuthority()
  {
    return this[symbol12a867c1352a7bc1f205b0c8c3f131d5a69929772dcf3f0487d5572d64a81c9a];
  }
  isAtLeast(authority)
  {
    let cond = false;
    switch(authority) {
      case "User":
        cond = cond || this.getAuthority()==="User";
      case "Moderator":
        cond = cond || this.getAuthority()==="Moderator";
      case "Admin":
        cond = cond || this.getAuthority()==="Admin";
      case "MasterAdmin":
        cond = cond || this.getAuthority()==="MasterAdmin";
      default:
        break;
    }
    return cond;
  }
  isAtMost(authority)
  {
    let cond = false;
    switch(authority) {
      case "MasterAdmin":
        cond = cond || this.getAuthority()==="MasterAdmin";
      case "Admin":
        cond = cond || this.getAuthority()==="Admin";
      case "Moderator":
        cond = cond || this.getAuthority()==="Moderator";
      case "User":
        cond = cond || this.getAuthority()==="User";
      default:
        break;
    }
    return cond;
  }
  isEqualTo(authority) {
    return this.getAuthority()===authority;
  }
  isStrongerThan(authority)
  {
    let cond;
    switch(authority) {
      case "MasterAdmin":
        cond = this.isAtLeast("MasterAdmin");
        break;
      case "Admin":
        cond = this.isAtLeast("MasterAdmin");
        break;
      case "Moderator":
        cond = this.isAtLeast("Admin");
        break;
      case "User":
        cond = this.isAtLeast("Moderator");
        break;
      default:
        break;
    }
    return cond;
  }
  isWeakerThan(authority)
  {
    let cond;
    switch(authority) {
      case "MasterAdmin":
        cond = this.isAtMost("Admin");
        break;
      case "Admin":
        cond = this.isAtMost("Moderator");
        break;
      case "Moderator":
        cond = this.isAtMost("User");
        break;
      case "User":
        cond = this.isAtMost("User");
        break;
      default:
        break;
    }
    return cond;
  }
}
